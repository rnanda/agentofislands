package com.aol.restful.java.payload;

import java.util.HashMap;
import java.util.logging.Logger;

import com.aol.restful.java.location.Coordinate;
import com.aol.restful.java.location.LocationService;
import com.aol.restful.java.sim.SimImpl;

public class PayloadService implements Payload {

	public PayloadService(LocationService service) {
		payloads = new HashMap<Integer, PayloadImpl>();
		this.locationService = service; 
	}


	@Override
	public int addPayload(String name, Coordinate coordinate) {
		//TODO could use global id
		int id = payloads.size();
		
		PayloadImpl payload = new PayloadImpl(id,name, coordinate);
		payloads.put(id, payload);
		
		return id;
	}

	@Override
	public Coordinate getCoordinate(int pid) {
		
		return payloads.get(pid).getCoordinate();
	}
	
	//Private Methods
	private HashMap<Integer,PayloadImpl> payloads;
	private LocationService locationService;
	private final static Logger logger = Logger.getLogger(PayloadService.class.getName());

}
//End of file