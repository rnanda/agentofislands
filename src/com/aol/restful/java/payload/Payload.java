package com.aol.restful.java.payload;

import com.aol.restful.java.location.Coordinate;

public interface Payload {
	
	//TODO other properties
	
	/**
	 * 
	 * @param name
	 * @param Coordinate
	 * @return
	 */
	public int addPayload(String name, Coordinate Coordinate);
	
	/**
	 * 
	 * @return
	 */
	public Coordinate getCoordinate(int pid);

}
//End of File