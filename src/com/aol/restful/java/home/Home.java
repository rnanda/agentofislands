package com.aol.restful.java.home;

import com.aol.restful.java.location.Coordinate;

/**
 * Home interface
 * @author rnanda
 *
 */
public interface Home {
	
	/**
	 * 
	 * @param name
	 * @param Coordinate
	 * @return
	 */
	public int addHome(String name, Coordinate Coordinate);
	
	/**
	 * 
	 * @return coordinate of wall
	 */
	public Coordinate getCoordinate(int hid);
}
