/**
 * 
 */
package com.aol.restful.java.home;

import java.util.HashMap;
import java.util.logging.Logger;

import com.aol.restful.java.location.Coordinate;
import com.aol.restful.java.location.LocationService;
import com.aol.restful.java.sim.SimImpl;
import com.aol.restful.java.wall.WallImpl;

/**
 * @author rnanda
 *
 */
public class HomeService implements Home{

	
	public HomeService (LocationService locationService) {
		this.walls = new HashMap<Integer, WallImpl>();
		this.locationService = locationService;
	}
	
	@Override
	public int addHome(String name, Coordinate coordinate) {
		WallImpl wImpl = new WallImpl(walls.size(), name, coordinate);
		
		walls.put(walls.size() -1 , wImpl);
		
		return walls.size() - 1 ;
	}

	@Override
	public Coordinate getCoordinate(int wid) {
		return walls.get(wid).getCoordinate();
	}

	private HashMap<Integer,WallImpl> walls;
	private LocationService locationService;
	private final static Logger logger = Logger.getLogger(HomeService.class.getName());

}
