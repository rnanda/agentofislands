package com.aol.restful.java.sim;

import java.util.Date;
import java.util.HashMap;
import java.util.logging.Logger;

import com.aol.restful.java.agent.Agent;
import com.aol.restful.java.agent.AgentAction;
import com.aol.restful.java.agent.AgentActionType;
import com.aol.restful.java.agent.AgentService;
import com.aol.restful.java.home.Home;
import com.aol.restful.java.home.HomeService;
import com.aol.restful.java.location.Coordinate;
import com.aol.restful.java.location.LocationService;
import com.aol.restful.java.payload.Payload;
import com.aol.restful.java.payload.PayloadService;
import com.aol.restful.java.queue.QueueService;
import com.aol.restful.java.wall.Wall;
import com.aol.restful.java.wall.WallService;

/**
 * Represent an instance of simulation
 * @author rnanda
 *
 */
public class SimImpl implements Sim{
	
	public  SimImpl(LocationService service) {
		this.locationService = service;
	}
	

	@Override
	public int create(int id, String name) {
		// TODO Auto-generated method stub
		this.id = id;
		this.name = name;
		this.running = false;
		
		agentService   = new AgentService(this.locationService);
		payloadService = new PayloadService(this.locationService);
		wallService    = new WallService(this.locationService);
		homeService    = new HomeService(this.locationService);
		
		return this.id;
	}

	@Override
	public int start() {
		this.running = true;
		
		return 0;
	}

	
	@Override
	public int agentAction(int aid, AgentActionType action) {
		//Queue the agentAction
		AgentAction aAction = new AgentAction(aid,action, new Date());
		QueueService.enqueue(aAction, 0);
		
		return 0;
	}

	@Override
	public int simStep() {
		//Get All the Action of Agent
		return 0;
	}
	
	@Override
	public boolean isSimRunning() {

		return running;
	}

	@Override
	public String agentStatus(int aid) {
		//Delegate to agent service.
		return agentService.getStatus(aid);
	}


	@Override
	public int addAgent(byte mode, Coordinate coordinate) {	
		//Delegate to agent service.
		return agentService.createAgent(name, mode, coordinate);
	}


	@Override
	public int addWall(String name, Coordinate coordinate) {
		
		return wallService.addWall(name, coordinate);
	}


	@Override
	public int addPayload(String name, Coordinate coordinate) {
		
		return payloadService.addPayload(name, coordinate);
	}
	@Override
	public int addHome(String name, Coordinate coordinate) {
		
		return homeService.addHome(name, coordinate);
	}
	
	@Override
	public String getStatistics() {
		return "TODO";
	}
	
	@Override
	public String getName() {
		return this.name;
	}
	
	@Override
	public int getId() {
		return this.id;
	}
	
	@Override
	public int getNumberOfAgents() {
		return agentService.getNumberOfAgents();
	}
	
	private int id;
	private String name;
	private boolean running;
	private boolean simCreated = false;
	private boolean simStarted = false;
	
	//TODO - 
	private QueueService qService;
	
	//OR
	//Private HashMap<aid,QueueService)
	
	//Spring to inject the services
	private Agent agentService;
	private Wall  wallService;
	private Payload payloadService;
	private Home homeService;
	private LocationService locationService;
	private final static Logger logger = Logger.getLogger(SimImpl.class.getName());
}
