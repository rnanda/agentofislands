/**
 * 
 */
package com.aol.restful.java.sim;

import com.aol.restful.java.agent.AgentActionType;
import com.aol.restful.java.location.Coordinate;


/**
 * @author rnanda
 *
 */
public interface Sim {

	/**
	 * 
	 * @param id
	 * @param name
	 * @return
	 */
	public int create(int id, String name);

	//TODO Not clear about start return status
	
	/**
	 * 
	 * @return
	 */
	public int getId();
	
	//Run all the actions queued up
	//TODO if there are no actions ?
	public int start();
	
	/** 
	 * Sim status
	 * @return
	 */
	public boolean isSimRunning();
	
	/**
	 * Add the agent
	 * @param Coordinate
	 * @return Id of the newly created agent
	 */
	public int addAgent(byte mode, Coordinate coordinate);
	
	/**
	 * Queue up all the agents actions
	 * @return
	 */
	public int agentAction(int aid, AgentActionType action);
	
	/**
	 * Run the step 
	 * @return
	 */
	public int simStep();
	
	/**
	 * Return agent status in json
	 * @param aid
	 * @return
	 */
	public String agentStatus(int aid);
	
	//Location related Actions
	/**
	 * 
	 * @param wall
	 * @param coordinate
	 * @return
	 */
	public int addWall(String wallName, Coordinate coordinate);
	
	//Payload Related actions
	/**
	 * 
	 * @param payload
	 * @param coordinate
	 * @return
	 */
	public int addPayload(String name, Coordinate coordinate);
	
	/**
	 * 
	 * @param name
	 * @param coordinate
	 * @return
	 */
	public int addHome(String name, Coordinate coordinate);
	
	/**
	 * 
	 * @return
	 */
	public String getStatistics();
	
	/**
	 * 
	 * @return
	 */
	public String getName();
	
	/**
	 * 
	 * @return
	 */
	public int getNumberOfAgents();
}
