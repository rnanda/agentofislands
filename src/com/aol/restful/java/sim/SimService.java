/**
 * 
 */
package com.aol.restful.java.sim;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.logging.Logger;


import org.json.*;


import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.aol.restful.java.agent.AgentActionType;
import com.aol.restful.java.location.LocationService;
import com.aol.restful.java.util.*;

/**
 * @author rnanda
 *
 */
@Path("/api/simulations/")
public class SimService {

	/**
	 * Create sim with a valid name.
	 * @param postBody
	 * @return Code: 200
				{msg': 'Simulation "%s" created with environment "%s"!' % (edata[0], env_name),
				'status': 200,
				'simulationId': <sid>,
				'simulationData': {'id': <sid>, 'time': 0, 'NumAgents': <numAgents>, 'Statistics': <statistics>,
				'SimPoints': 0}
				}
				Code: 400 - Incorrect Name Specified
				Code: 415 - No or incorrectly formatted json payload
	 */
	
	@POST
	@Path("/create")
	@Produces("application/json")
	public Response createSim(String postBody) {
		
		//Check to make sure postBody has all the required data.
		if (Util.isEmptyOrNull(postBody)) {
			//Create a 415
			return createError(415,"415 - No or incorrectly formatted json payload");
		}
		
		//Check if there is env_name
		JSONObject json = null;
		String name  = null;
		try {
			json = new JSONObject(postBody);
		
			name = json.getString("env_name");
		} catch (Exception e) {
			return createError(415,"415 - No or incorrectly formatted json payload");
		}
		
		//Check if env is not null and only allowed 2 values
		if (name == null || name.isEmpty()) {
			//return error
		} else if (name.compareTo("Test1") != 0 ||
				   name.compareTo("Test2") != 0 ) {
			return createError(400,"400 - Incorrect Name Specified");
		}
		
		Sim sim = new SimImpl(SimService.locationService);
		sim.create(sims.size(), name);
		
		//create Response object 
	
		String pattern = "Simulation \"%s\" created with environment \"%s\"!\'";
		String msg = String.format(pattern, sims.size(), "Success");
		try {
			json = new JSONObject();
			json.put("msg", msg);
			
			json.put("status", 200);
			json.put("simulationId", sim.getId());
			
			JSONObject json2 = new JSONObject();
			json2.put("id", sim.getId());
			json2.put("time", 0);
			json2.put("agents", 0);
			json2.put("Statistics", sim.getStatistics());
			
			json.put("simulationData", json2);
			json.put("SimPoints", 0);
			
		}catch (Exception e) {
			return createError(400,"400 - Internal Application Error");
		}
		
		return Response.status(200).type(MediaType.APPLICATION_JSON).entity(json.toString()).build();
	}
	
	/**
	 * 
	 * @return
	 */
	@PUT
	@Path("/{sid}/start")
	@Produces("application/json")
	public Response startSim(@PathParam("sid") int sid) {
		Sim sim = sims.get(sid);
		if (sim == null) {
			return Response.status(400).entity("400 - Incorrect Name Specified").build();
		}
	
		sim.start();
		
		JSONObject json  = new JSONObject();
		JSONObject json2 = new JSONObject();
		
		String msg = "Simulation " + sid + " restarted!";

		try {
			json.put("msg", msg );
			json.put("status", 200);
			json2 = new JSONObject();
			json2.put("id", sid);
			json2.put("time", new Date().toString());
			json2.put("numAgents", sim.getNumberOfAgents());
			json.put("simulationData", json2);
		} catch(Exception e) {
			return Response.status(400).type(MediaType.TEXT_PLAIN).entity("Interal Application Error").build();
		}
		
		return Response.status(200).entity(json.toString()).build();
	}
	
	
	@GET
	@Path("/{sid}/agents/{aid}/status")
	@Produces("application/json")
	public Response agentStatus(@PathParam("sid") int sid, 
							    @PathParam("aid") int aid) {
		Sim sim = sims.get(sid);
		if (sim == null) {
			return Response.status(400).entity("400 - Incorrect Name Specified").build();
		}
		
		return Response.status(200).type(MediaType.APPLICATION_JSON).entity("").build();
	}
	
	@POST
	@Path("/{sid}/agents/{aid}/action")
	@Produces("application/json")
	public Response agentAction(@PathParam("sid") int sid, 
			  				    @PathParam("aid") int aid,
			  				    String postBody) {	
		Sim sim = sims.get(sid);
		if(sim == null) {
			createError(400,"400 - Incorrect Name Specified");
		}
		JSONObject json = null;
		String action  = null;
		
		int mode = 0;
		try {
			json = new JSONObject(postBody);
		
			action = json.getString("action");
			mode = json.getInt("mode");
		} catch (Exception e) {
			return createError(415,"415 - No or incorrectly formatted json payload");
		}
		
		if(action == null || action.isEmpty()) {
			return createError(415,"415 - No or incorrectly formatted json payload");
		}
		AgentActionType aType = null;
		try {
			aType = AgentActionType.valueOf(action);
		}catch(Throwable th) {
			return createError(415,"415 - No or incorrectly formatted json payload");
		}
		
		//enqueue the actions 
		//?
		
		
		//Response 
		json = new JSONObject();
		try {
			sim.agentAction(aid, aType);
			
			json.put("msg", "Action submitted");
			json.put("status", 200);
			json.put("data", "None");
		} catch(Exception e) {
			return Response.status(400).type(MediaType.TEXT_PLAIN).entity("Interal Application Error").build();	
		}
	
		return Response.status(200).type(MediaType.APPLICATION_JSON).entity(json.toString()).build();
	}
	
	/**
	 * 
	 * @return
	 */
	@PUT
	@Path("/{sid}/agents/{aid}/simStep")
	@Produces("application/json")
	public Response simStep(@PathParam("sid") int sid, 
			  			    @PathParam("aid") int aid) {
		//Can we start a Thread and execute all in parallel 
		Sim sim = sims.get(sid);
		if(sim == null) {
			createError(400,"400 - Incorrect Name Specified");
		}
		
		//execute all the actions for the sim;
		sim.simStep();
		
		JSONObject json = new JSONObject();
		try {
			json.put("msg", "Action simulated");
			json.put("status",200);
			json.put("data", "None");
		} catch(Exception e) {
			return Response.status(400).type(MediaType.TEXT_PLAIN).entity("Interal Application Error").build();	
		}
		
		return Response.status(200).entity("").build();
	}

	
	/**
	 * 
	 * @return
	 */
	public boolean isSimStarted(int sid) {
		
		return sims.get(sid).isSimRunning();
	}
	
	/**
	 * 
	 * @return
	 */
	public boolean isSimCreated(int sid) {
		Sim sim = sims.get(sid);
		return sim == null;
	}
	
	/**
	 * 
	 * @param obj
	 * @return
	 */
	private String toJson() {
		
		return "";
	}
	
	
	private Response createError (int responseCode, String msg) {
		return Response.status(responseCode).encoding(msg).build();
	}
	
	
	//Private Static member variable
	private static HashMap<Integer,Sim> sims = new HashMap<Integer, Sim>(); 

	private static LocationService locationService = new LocationService();
	
	final static Logger logger = Logger.getLogger(SimService.class.getName());

}
//