package com.aol.restful.java.location;

import java.util.logging.Logger;

/**
 * 
 * @author rnanda
 *
 */
public class ObjectTypeContainer {
	public ObjectTypeContainer(ObjectType objectType,Coordinate coordinate) {
		this.objectType = objectType;
		this.coordinate = coordinate;
	}
	
	public ObjectType getObjecType() {
		return objectType;
	}
	
	public Coordinate getCoordinate() {
		return this.coordinate;
	}
	
	
	//Private Data Members
	private ObjectType objectType;
	private Coordinate coordinate;
	private final static Logger logger = Logger.getLogger(ObjectTypeContainer.class.getName());
}
//End Of Class