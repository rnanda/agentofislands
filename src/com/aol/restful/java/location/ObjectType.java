package com.aol.restful.java.location;

public enum ObjectType {
	Empty,
	Agent,
	Home,
	Wall,
	Payload
}
