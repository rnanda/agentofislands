package com.aol.restful.java.location;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Logger;

import com.aol.restful.java.sim.SimImpl;

public class LocationService implements Location{
	 

	@Override
	public ObjectTypeContainer objectAtLocation(Coordinate coordinate) {
		
		return locations.get(coordinate);
	}

	@Override
	public int place(Coordinate coordinate, ObjectType type) {
		//Assumption placing the object on the location spot, caller must ensure 
		locations.put(coordinate, new ObjectTypeContainer(type,coordinate));
		
		return 0;
	}
	@Override 
	public int hidePayload(Coordinate coordinate) {
		locations.remove(coordinate);
		return 0;
	}
	
	@Override
	public ObjectTypeContainer[] scanObjects(Coordinate coordinate, int scanDistance) {
		Iterator it = this.locations.entrySet().iterator();
		ArrayList<ObjectTypeContainer> objects = new ArrayList<ObjectTypeContainer>();
		ObjectTypeContainer oContainer = null;
		while (it.hasNext()) {
			//get the coordinate and check if this is within the scan distance.
			//Calculate the Manhattan Distance
			oContainer = (ObjectTypeContainer) it.next();
			if (manhattanDistance(oContainer.getCoordinate(), scanDistance)){
				objects.add(oContainer);
			}
		}
		return (ObjectTypeContainer[])objects.toArray();
	}
	
	private boolean manhattanDistance(Coordinate coordinate, int scanDistance) {
		
		return true;
	}
	
	private HashMap<Coordinate,ObjectTypeContainer> locations = new HashMap<Coordinate , ObjectTypeContainer>();
	private final static Logger logger = Logger.getLogger(LocationService.class.getName());
}
//End Of File