/**
 * 
 */
package com.aol.restful.java.location;

/**
 * @author rnanda
 *
 */
public interface Location {

	/**
	 * 
	 * @param coordinate
	 * @param type
	 * @return Null empty,
	 * other Object type caller will decide if they can place 
	 */
	
	public ObjectTypeContainer objectAtLocation(Coordinate coordinate);
	
	/**
	 * 
	 * @param coordinate
	 * @param type
	 * @return
	 */
	public int place(Coordinate coordinate,ObjectType type);
	
	/**
	 * Take the payload out
	 * @param coordinate
	 * @return
	 */
	public int hidePayload(Coordinate coordinate);
	
	/**
	 * 
	 * @param scanDistance
	 * @return
	 */
	public ObjectTypeContainer[] scanObjects(Coordinate coordinate, int scanDistance);
}
