/**
 * 
 */
package com.aol.restful.java.location;

import java.util.HashMap;
import java.util.logging.Logger;

import com.aol.restful.java.sim.SimImpl;

/**
 * @author rnanda
 *
 */
public class LocationImpl implements Location {

	public LocationImpl() {
		this.locationMap = new HashMap<Coordinate,ObjectTypeContainer>();
	}
	
	@Override
	public int place(Coordinate coordinate, ObjectType type) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public ObjectTypeContainer[] scanObjects(Coordinate coordinate, int scanDistance) {
		// TODO Auto-generated method stub
		return null;
	}
	
	//@Override
	public ObjectTypeContainer objectAtLocation(Coordinate coordinate) {
		
		return this.locationMap.get(coordinate);
	}

	//@Override
	public int place(Coordinate coordinate, ObjectTypeContainer type) {
		
		this.locationMap.put(coordinate, type);
		return 0;
	}

	//@Override
	public ObjectTypeContainer[] scanObjects(int scanDistance) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public int hidePayload(Coordinate coordinate) {
		this.locationMap.remove(coordinate);
		return 0;
	}


	private HashMap<Coordinate, ObjectTypeContainer> locationMap;

	private final static Logger logger = Logger.getLogger(LocationImpl.class.getName());


}
//End of Class
