package com.aol.restful.java.location;

import java.util.logging.Logger;

public class Coordinate {
	
	public Coordinate(int x, int y) {
		this.x = x;
		this.y = y;
		this.z =(int)Math.random() * 100000;
	}
	public int moveForward(char direction) {
		//check current coordinate and direction 
		switch(direction) {
			case 'F': x++;y++;
					break;
			case 'L':
					break;
			case 'R':
					break;
			case 'B':
					break;
		}
		return 0;
	}
	public int moveLeft(char direction) {
		//check current coordinate and direction 
		switch(direction) {
			case 'F': x--;y--;
					break;
			case 'L':
					break;
			case 'R':
					break;
			case 'B':
					break;
		};
		return 0;
	}
	public int moveRight(char direction) {
		//check current coordinate and direction 
		switch(direction) {
			case 'F': x++;y++;
					break;
			case 'L':
					break;
			case 'R':
					break;
			case 'B':
					break;
		}
		return 0;
	}
	
	
	@Override
	public String toString() {
		return new StringBuilder(x).append(y).append(z).toString();
	}
	
	@Override
	public int hashCode() {
		//Make Sure Unique code is returned
		return x + y + z;
	}
	
	//TODO Find a better way to make sure its a unique hash code
	private int x,y,z;
	private final static Logger logger = Logger.getLogger(Coordinate.class.getName());	
}
//End Of File