/**
 * 
 */
package com.aol.restful.java.queue;

import java.util.Queue;
import java.util.concurrent.PriorityBlockingQueue;

/**
 * @author rnanda
 *
 */
public class QueueService {
	
	public static boolean enqueue (Object obj,int priorityj) {
		
		queue.add(obj);
		
		return true;
	}
	
	/**
	 * 
	 * @param priority
	 * @return null if queue is empty;
	 */
	public static Object dequeue (int priority) {
		
		return queue.poll();
	}
	
	//This can be changed to other implementation with Visitor pattern
	private static Queue<Object> queue = new PriorityBlockingQueue<Object>();
	
}
