package com.aol.restful.java.agent;

import java.util.Date;

public class AgentAction {
	public AgentAction(int aid, AgentActionType actionType, Date date) {
		this.aid = aid;
		this.agentActionType = actionType;
		this.date = date;
	}
	
	public int aid;
	public AgentActionType agentActionType;
	public Date date;
}
