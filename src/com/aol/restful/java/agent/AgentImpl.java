package com.aol.restful.java.agent;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.ws.rs.core.MediaType;

import org.json.JSONObject;

import com.aol.restful.java.agent.Agent.AgentResponseCode;
import com.aol.restful.java.location.Coordinate;
import com.aol.restful.java.location.LocationService;
import com.aol.restful.java.location.ObjectTypeContainer;
import com.aol.restful.java.sim.SimImpl;

/**
 * Agent implementation handles all the agents work. 
 * @author rnanda
 *
 */
public class AgentImpl {

	public AgentImpl(int aid, String name, Coordinate coordinate, 
					byte mode, LocationService service) {
		this.aid = aid;
		this.coordinate = coordinate;
		this.name = name;
		this.locationService = service;
		this.score = 0;
		this.mode = mode;
		this.direction = 'F';
	}
	
	/**
	 * 
	 * @param agentAction
	 * @return
	 */
	public AgentResponseCode action(AgentActionType agentAction, byte mode) {
		this.mode = mode;
		this.currentActionType = agentAction;
		AgentResponseCode status = AgentResponseCode.success;
		switch(agentAction) {
			case moveForward:
				
				direction = 'F';
				calculateScore();
				break;
			case turnLeft:
			
				direction = 'L';
				
				calculateScore();
				break;
			case turnRight:
				
				
				//Check if Agent is carrying a load
				
				this.coordinate.moveRight(this.direction);
				direction = 'R';
				calculateScore();
				break;
			case pickup:
							
				if (this.payload) {
					//agent is carry a payload
					return AgentResponseCode.alreadyCarryPayload;
				}
				//LocationService check if move
				
				//pickup the pay load & update the mode status
				
				payload = true;
				//Check the Payload information. Is it already assigned to other agent
				
				//Mark the load in location service to 
				locationService.hidePayload(this.coordinate);
				
				calculateScore();
				break;
			case drop: 
				
				//Check if this location has home
				//Change the status of the payload to delivered. 
				calculateScore();
		    	break;
			case idle: 
				
				//No Score
		    	break;
		}
		
		String msg;
		if (status == AgentResponseCode.success) {
			msg = "Success";
		} else {
			msg = "Failure";
		}
		
		//update the Last action status
		//TODO - Date must be from a util clock synchronized 
		ActionStatus agentStatus = new ActionStatus(agentAction, status, msg , new Date());
		
		actionList.add(agentStatus);
		
		//update the status
		this.currentActionType = AgentActionType.idle;

		return status;
	}
	
	/**
	 * 
	 * @return
	 */
	public ActionStatus getLastAction() {
		//make sure its not empty
		if(actionList.isEmpty()) {
			return null;
		}
		
		return actionList.get(actionList.size()-1);
	}
	
	/**
	 * 
	 * @return
	 */
	public String getStatus(String type) {
		//Return the JSON but can depends on type
		
		//create the json string
		//Scan for walls , homes, payloads, agentScan
		
		/*
		 * {'msg': 'Done',
			'status': 200,
			'agentData': {"Status":{"LastAction" : <lastAction>, "LastStatus":<lastStatus>,
			"Mode":<modeInt>}, "Scan": {"Walls":<WallScan>, "Home":<HomeScan>,
			"Payloads":<PayloadScan>, "Agents":<AgentScan>}}
		 */
		JSONObject json = new JSONObject();
		JSONObject json2 = new JSONObject();
		JSONObject json3 = new JSONObject();
		try {
			json.put("msg", "Done").put("status", 200);
			json2.put("LastAction", actionList.get(actionList.size()-1).getActionType());
			json2.put("LastStatus", actionList.get(actionList.size()-1).getStatus());
			json2.put("Mode",this.mode);
			
		} catch(Exception e) {
			
		}
		
		return null;
	}
	
	/**
	 * 
	 * @return
	 */
	public boolean hasPayload() {
		return payload;
	}
	
	/**
	 * 
	 * @return
	 */
	public int getScore() {
		return score;
	}
	
	//Scan the types 
	//Home is static
	private int manhattanScan() {
		
		return 0;
	}
	
	private void calculateScore() {
		
	}
	
	private class ScanData {
		//"Scan": {"Walls":<WallScan>, "Home":<HomeScan>,
		//"Payloads":<PayloadScan>, "Agents":<AgentScan>}}
		public void scanAndParse(Coordinate coordinate, int scanDistance) {
			ObjectTypeContainer[] objects = locationService.scanObjects(coordinate, scanDistance);
		}
		public Coordinate[] walls;
		public Coordinate[] homes;
		public Coordinate[] payloads;
		public Coordinate[] agents;
	};
	
	//Private Members
	private char direction;
	private Coordinate coordinate;
	private AgentActionType currentActionType;
	private String name;
	private int score;
	private int median;
	private int min;
	private int max;
	private int average;
	private int aid;
	private List<ActionStatus> actionList = new ArrayList<ActionStatus>();
	private boolean payload;
	private ScanData scanData;
	
	//Payload carrying #
	private byte mode;
	
	private LocationService locationService;
	private final static Logger logger = Logger.getLogger(AgentImpl.class.getName());
}
//End Of File