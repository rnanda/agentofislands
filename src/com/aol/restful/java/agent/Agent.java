package com.aol.restful.java.agent;

import com.aol.restful.java.location.Coordinate;

public interface Agent {
	
	/**
	 * spec here all the agent action
	 * @param action
	 * @return
	 */
	public int createAgent(String name, byte mode, Coordinate coordinate);
	
	/**
	 * 
	 * @param aid
	 * @return Json 
	 */
	public String getStatus(int aid);
	
	/**
	 * Return agent engery score
	 * @param aid
	 * @return
	 */
	public int getScore(int aid);
	
	
	//TODO - queue should be at service or agent level
	public int queueAction(int aid, AgentActionType action);
	
	
	//priority might be used in future.
	public AgentResponseCode runAction(int aid, int priority);
	
	public int getNumberOfAgents();
	
	public boolean hasPayload(int aid);
	
	public String getStats();
	
	public static enum AgentResponseCode {
		success,
		noPayload,
		alreadyCarryPayload,
		cantMoveAgent,
		cantMoveWall,
		cantMoveHome,
		cantMovePayload
	}
	//TODO Delegate 
}
//End of file