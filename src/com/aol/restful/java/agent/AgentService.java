package com.aol.restful.java.agent;

import java.util.HashMap;
import java.util.logging.Logger;

import com.aol.restful.java.location.Coordinate;
import com.aol.restful.java.location.LocationService;

public class AgentService implements Agent {

	public AgentService (LocationService locationService) {
		agents = new HashMap<Integer,AgentImpl>();
		this.locationService = locationService;
	}

	@Override
	public String getStatus(int aid) {
		String status = agents.get(aid).getStatus("Json");
		return status;
	}

	@Override
	public int getScore(int aid) {
		
		return agents.get(aid).getScore();
	}
	
	@Override
	public String getStats() {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public int createAgent(String name, byte mode, Coordinate coordinate) {
		// TODO Auto-generated method stub
 
		AgentImpl agent = new AgentImpl(agents.size(), name, coordinate,
										mode, this.locationService);
		
		agents.put(agents.size(), agent);
		
		return agents.size()-1;
	}

	@Override
	public int queueAction(int aid, AgentActionType action) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public AgentResponseCode runAction(int aid, int priority) {
		// TODO Auto-generated method stub
		return AgentResponseCode.success;
	}

	public int getNumberOfAgents() {
		return agents.size();
	}

	public boolean hasPayload(int aid) {
		return agents.get(aid).hasPayload();
	}
	
	//Private Members
	private HashMap<Integer, AgentImpl> agents;
	private LocationService locationService;
	private final static Logger logger = Logger.getLogger(AgentService.class.getName());

}
