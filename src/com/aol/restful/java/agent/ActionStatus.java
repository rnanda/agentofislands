package com.aol.restful.java.agent;

import java.util.Date;
import java.util.logging.Logger;

import com.aol.restful.java.agent.Agent.AgentResponseCode;
import com.aol.restful.java.sim.SimImpl;


/**
 * Action Status History
 * @author rnanda
 *
 */
public class ActionStatus {

	public ActionStatus(AgentActionType action, AgentResponseCode status, String msg, Date date) {
		this.actionType = action;
		this.status = status;
		this.date = date;
		this.message = msg;
	}
	 
	
	public AgentActionType getActionType() {
		return actionType;
	}
	
	public AgentResponseCode getStatus() {
		return status;
	}
	
	public Date getDate() {
		return date;
	}
	
	public String getMessage() {
		return message;
	}

	private AgentActionType actionType;
	private AgentResponseCode status;
	private Date date; 
	private String message;
	private final static Logger logger = Logger.getLogger(ActionStatus.class.getName());
}
//End of file