/**
 * 
 */
package com.aol.restful.java.agent;

/**
 * @author rnanda
 *
 */
public enum AgentActionType {
	moveForward,
	turnLeft,
	turnRight,
	pickup,
	drop,
	idle
}
