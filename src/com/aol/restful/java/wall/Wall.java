package com.aol.restful.java.wall;

import com.aol.restful.java.location.Coordinate;

public interface Wall {
	/**
	 * 
	 * @param name
	 * @param Coordinate
	 * @return
	 */
	public int addWall(String name, Coordinate Coordinate);
	
	/**
	 * 
	 * @return coordinate of wall
	 */
	public Coordinate getCoordinate(int wid);
}
