package com.aol.restful.java.wall;

import java.util.logging.Logger;

import com.aol.restful.java.location.Coordinate;
import com.aol.restful.java.sim.SimImpl;

public class WallImpl {
	
	public WallImpl(int id, String name, Coordinate coordinate) {
		this.id = id;
		this.name = name;
		this.coordinate = coordinate;
	}

	public int getId() {
		return this.id;
	}
	
	public String getName () {
		return this.name;
	}
	
	public int setCoordinate(Coordinate coordinate) {
		this.coordinate = coordinate;
		
		return 0;
	}
	
	public Coordinate getCoordinate() {
		return this.coordinate;
	}
	
	//Private data members
	private int id;
	private String name;
	private Coordinate coordinate; 
	private final static Logger logger = Logger.getLogger(WallImpl.class.getName());

}
