Overview

Island of Agents is a simulation puzzle that you interact with via a REST interface and code that you write.
The island consists of 4 types of entities: Agents, Payloads, Home, Walls. Each location on the island will
be one of these entities or empty. Agents are able to move and manipulate payloads based on your
commands. Your job is to write the logic to control the agents to carry out tasks on the island as assigned
in the Challenges section below.
This is exercise is meant to give an opportunity to show creative thinking, problem solving and
demonstration of craft with the idea that it will give us a window into what makes you tick. In order to make
the situation realistic, the documentation may be a little sketchy or code may have bugs just like in real life.
When you are done, you will be expected to walk us through your code and tell us about your thought
processes to help us understand your problem solving and development process.